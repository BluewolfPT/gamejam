using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FrangoEaster : MonoBehaviour
{
    public List<GameObject> easterEggs = new List<GameObject>();
    void Start()
    {
        StartCoroutine(ActiveC());
    }

    
    void Update()
    {
        
    }

    IEnumerator ActiveC()
    {
        int i = Random.Range(0, easterEggs.Count);
        easterEggs[i].SetActive(true);
        yield return new WaitForSeconds(5);
        easterEggs[i].SetActive(false);
        StartCoroutine(ActiveC());
    }
}
