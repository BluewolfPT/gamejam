using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatMenu : MonoBehaviour
{
    Rigidbody2D rb;
    public int force;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        Time.timeScale = 1;

        float x = Random.Range(-1, 1);
        float y = Random.Range(-1, 1);

        rb.AddForce(new Vector2(0.5f + x, 0.5f + y).normalized * force, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
