using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour
{
    Player pl;
    void Start()
    {
        pl = Player.instance;
    }

    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            pl.Vida++;
            Destroy(gameObject);
        }

    }

}
