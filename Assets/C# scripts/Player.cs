using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IInteractable
{
    #region Variaveis
    [SerializeField]
    float speed = 2f;
    [SerializeField]
    float geyserForce;
    [SerializeField]
    float jumpForce = 2f;
    [SerializeField]
    private int jumpNumber = 2;
    private int jumpNumberCounter;

    private bool doJump;
    private bool canJump = true;

    bool facingRight = true;

    private int vida;
    [SerializeField]

    private int maxVida = 3;
    public int Vida
    {
        get
        {
            return vida;
        }

        set
        {
            vida = value;
            if (vida >= maxVida)
            {
                vida = maxVida;
            }
            else if (vida <= 0)
            {
                vida = 0;
                gm.GameOver(false);
            }

            ui.UpdateVida(vida,maxVida);
        }
    }

    UIManager ui;
    GameManager gm;
    Rigidbody2D rb;
    Animator anim;
    #endregion

    #region Singleton
    public static Player instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
            instance = this;
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>(); 
        ui = UIManager.instance;
        gm = GameManager.instance;
        vida = maxVida;
    }

    // Update is called once per frame
    void Update()
    {

        if (canJump && Input.GetButtonDown("Jump") && jumpNumberCounter < jumpNumber)
        {
            jumpNumberCounter++;
            doJump = true;
        }
    }
    void FixedUpdate()
    {
        Move();
        if (doJump)
        {
            doJump = false;
            Jump();
        }
    }

    private void Move()
    {
        float x = Input.GetAxisRaw("Horizontal");
        transform.Translate(Vector2.right * speed * x * Time.deltaTime);
        anim.SetFloat("speed", Mathf.Abs(x));
        Flip(x);
    }

    private void Jump()
    {    
        rb.AddForce(Vector2.up * jumpForce * jumpNumber / jumpNumberCounter, ForceMode2D.Impulse);
        anim.SetTrigger("jump");
    }

    public void TakeDamage()
    {
        Vida = Vida - 1;
        anim.SetTrigger("gotHit");
    }

    void Flip(float x)
    {
        if (x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
            facingRight = !facingRight;
        }
        else if (x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
            facingRight = !facingRight;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            canJump = true;
            jumpNumberCounter = 0;
        }
    }

    public void Interact(int direcao)
    {
        if (direcao == 1)
            rb.AddForce(Vector3.right * geyserForce);
        else if (direcao == 2)
            rb.AddForce(Vector3.up * geyserForce);
        else if (direcao == 3)
            rb.AddForce(Vector3.left * geyserForce);

    }
}
