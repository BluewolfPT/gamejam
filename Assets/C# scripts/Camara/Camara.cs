﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
     Transform playerTransform;
    
    [SerializeField]
    Vector3 offset;

    [SerializeField]
     float dampTime = 0.15f;

    private Vector3 velocity = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        SetPlayer();
    }
    void SetPlayer()
    {
        playerTransform = GameObject.Find("Player").transform;
    }


    // Update is called once per frame
    void Update()
    {
        Follow();
    }
    void Follow()
    {
        transform.position = Vector3.SmoothDamp(transform.position, playerTransform.position + offset, ref velocity, dampTime);
    }
    
       
}
