﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour
{
    private float time;
    private float interval;
    float one;

    public bool timerStart;

    UIManager ui;

    #region Singleton
    public static Timer instance;

    public float Timecounter { get => time; set => time = value; }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
            instance = this;
    }
    #endregion

    private void Start()
    {
        ui = UIManager.instance;
        timerStart = false;
        one = 1;
    }

    private void Update()
    {
        if(timerStart == true)
        IsTime();
    }

    public void BeginTimer(float interval){
        
		this.interval = interval;
        timerStart = true;
	}

	public void IsTime()
    {
        one = one - Time.deltaTime;

        if(one <= 0)
        {
            one = 1;
            interval--;
        }
        
        if (interval < 0)
            interval = 0;

        ui.UpdateTimer(interval);
	}
}
