using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    #region Variables
    #endregion

    private void Start()
    {
        Time.timeScale = 1;
    }


    public void PlayGame()
    {
        Invoke("PGTimer",1);
        print("1 sec");
    }
    public void MainMenuLoader()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        print("done");
    }
    public void QuitGame()
    {
        Invoke("QGame",1);
        print("1 sec");
    }
    public void HowToPlay()
    {
        Invoke("HTPTimer",1);
        print("1 sec");
    }
    private void PGTimer()
    {
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        print("done");
    }
    private void HTPTimer()
    {
        SceneManager.LoadScene("HowToPlay", LoadSceneMode.Single);
        print("done");
    }
    private void QGame()
    {
        Application.Quit();
        print("done");
    }
    public void QuitGameNoTimer()
    {
        Application.Quit();
        print("done");
    }
    public void ReplayGame()
    {
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        print("done");
    }
}
