using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]

public class geysersX : MonoBehaviour
{

    #region Variables
    [SerializeField]
    protected float maxWaitingTimer = 5;
    protected float waitingTimer;
    [SerializeField]
    protected float maxGeyserTimer = 2;
    protected float geyserTimer;
    protected BoxCollider2D colligion;
    protected float variavel;
    [SerializeField]
    protected float howBig;
    protected int direcao;
    protected bool onTheGeyser = false;
    Player player;
    protected enum State
    {
        WAITTIME,
        EXPLOSION
    }
    protected State state;
    #endregion

    protected void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        colligion = GetComponent<BoxCollider2D>();
        colligion.isTrigger = true;
        state = State.WAITTIME;
        waitingTimer = maxWaitingTimer;
        geyserTimer = maxGeyserTimer;
    }


    protected void Update()
    {
        Evaluator();
        GeyserInteraction();
    }
    protected virtual void Evaluator()
    {
        switch (state)
        {
            case State.WAITTIME:
                colligion.size = new Vector2(0, colligion.size.y);

                waitingTimer = waitingTimer - Time.deltaTime;
                if (waitingTimer <= 0)
                {
                    state = State.EXPLOSION;
                    waitingTimer = maxWaitingTimer;
                }

                break;
            case State.EXPLOSION:

                variavel = colligion.size.x;
                variavel = variavel + (howBig * Time.deltaTime);
                colligion.size = new Vector2(variavel ,colligion.size.y);

                geyserTimer = geyserTimer - Time.deltaTime;
                if (geyserTimer <= 0)
                {
                    geyserTimer = maxGeyserTimer;
                    state = State.WAITTIME;
                }
                break;
        }
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        IInteractable player = collision.GetComponent<IInteractable>();
        if (player != null)
        {
                direcao = 0;
            if (transform.rotation.z < 0)
                direcao = 1;
            else if (transform.rotation.z == 0)
                direcao = 2;
            else if (transform.rotation.z > 0)
                direcao = 3;

            onTheGeyser = true;

        }
    }
    protected void OnTriggerExit2D(Collider2D collision)
    {
        onTheGeyser = false;
    }
    protected void GeyserInteraction()
    {
        if (onTheGeyser)
            player.Interact(direcao);
    }





}
