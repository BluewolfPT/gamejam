using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Variaveis
    [SerializeField]
    GameObject gameUI; // meter aqui o canvas da vida dele e coisas assim para desaparecerem no pause e endgame
    
    [SerializeField]
    GameObject pausedMenu;
    
    [SerializeField]
    GameObject endMenuVitoria;
   
    [SerializeField]
    GameObject endMenuDerrota;
    
    [SerializeField]
    Image vida;
    
    [SerializeField]
    Image vidaFill;

    [SerializeField]
    TextMeshProUGUI timer;

    #endregion
    #region Singleton
    public static UIManager instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
            instance = this;
    }
    #endregion
    void Start()
    {
        gameUI.SetActive(true);
        pausedMenu.SetActive(false);
        endMenuVitoria.SetActive(false);
        endMenuDerrota.SetActive(false);
        timer.text = " ";
    }
    void Update()
    {
        
    }

    public void PausedGame(bool paused)
    {
        if (paused == true)
        {
            pausedMenu.SetActive(true);
            gameUI.SetActive(false);
        }
        else
        {
            pausedMenu.SetActive(false);
            gameUI.SetActive(true);
        }
    }
    
    public void EndGame(bool vitoria)
    {
        if( vitoria == true)
        {
            endMenuVitoria.SetActive(true);
            gameUI.SetActive(false);
        }
        else
        {
            endMenuDerrota.SetActive(true);
            gameUI.SetActive(false);
        }
    }

    public void UpdateVida(int vida, int maxVida)
    {
        vidaFill.fillAmount = (vida / maxVida);
    }

    public void UpdateTimer(float timer)
    {
        this.timer.text = "" + timer;
    }

    public void DeactivateTimer()
    {
        timer.text = " ";
    }

}
