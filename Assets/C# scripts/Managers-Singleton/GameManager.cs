using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Variaveis
    bool pause;
    
    UIManager ui;
    #endregion
    #region Singleton
    public static GameManager instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
            instance = this;
    }
    #endregion
    // Start is 
    // Start is called before the first frame update
    void Start()
    {
        ui = UIManager.instance;
        Time.timeScale = 1;
        pause = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pausing();
        }
    }

    void Pausing()
    {
        pause = !pause;
        Time.timeScale = 0;
        ui.PausedGame(pause);
    }

    public void GameOver(bool vitoria)
    {
        Time.timeScale = 0;
        ui.EndGame(vitoria);
    }
}
