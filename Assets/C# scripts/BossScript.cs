using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : Enemy
{
    #region Variables

    GameObject[] Platforms;

    Transform target;

    bool playerIsInTheHouse;
    float speed;

    public int HP
    {
        get
        {
            return HP;
        }
        set
        {
            HP = value;
            if (HP >= 3)
                HP = 3;
            else if (HP < 0)
                HP = 0;
        }
    }

    #endregion
    enum State
    {
        Idle,
        NormalAttack,
        RageAttack,
        Jump
    }

    State state;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine();
    }

    void StateMachine()
    {
        switch (state)
        {
            case State.Idle:
                if(playerIsInTheHouse == true)
                {
                    IdleState();
                }
                else
                {

                }
            break;

            case State.NormalAttack:

                target = GameObject.Find("Player").GetComponent<Transform>();

                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed);

                break;

            case State.RageAttack:

            break;

            case State.Jump:

                JumpToPlatform();

            break;
        }
    }

    IEnumerator IdleState()
    {
        yield return new WaitForSeconds(6);

        if (HP >= 3)
        {
            state = State.NormalAttack;
        }
        if(HP <= 2)
        {
            float random = Random.Range(0, 100f);

            if(random >= 50)
            {
                state = State.NormalAttack;
            }
            else
            {
                state = State.RageAttack;
            }
        }
    }

    void JumpToPlatform()
    {
        int random = Random.Range(0, Platforms.Length - 1);

        transform.position = Vector3.MoveTowards(transform.position, Platforms[random].transform.position, speed);
    }
}
