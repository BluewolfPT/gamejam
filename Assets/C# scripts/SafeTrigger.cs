using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeTrigger : MonoBehaviour
{
    UIManager ui;
    Timer counter;

    void Start()
    {
        ui = UIManager.instance;
        counter = Timer.instance;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            counter.timerStart = false;
            ui.DeactivateTimer();
            Destroy(this);
        }
    }
}
