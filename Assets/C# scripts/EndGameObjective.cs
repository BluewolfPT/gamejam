using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameObjective : MonoBehaviour
{
    GameManager gm;
    void Start()
    {
        gm = GameManager.instance;
    }

    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            gm.GameOver(true);
            Destroy(gameObject);
        }

    }
}
