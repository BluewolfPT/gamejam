using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    #region Variaveis
    [SerializeField]
    float speed = 3f;

    [SerializeField]
    Transform groundCheckerTarget;

    [SerializeField]
    float bounceForce = 18f;

    bool isGrounded;
    #endregion

    void Update()
    {
        Movement();
        isGrounded = GroundChecker();
    }

    void Movement()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
    bool GroundChecker()
    {
        RaycastHit2D ray = Physics2D.Linecast(transform.position, groundCheckerTarget.position, 1 << LayerMask.NameToLayer("Floor"));

        if (ray.collider != null)
        {
            transform.SetParent(ray.collider.transform);
            Debug.DrawLine(transform.position, groundCheckerTarget.position, Color.green);
            return true;
        }
        else
        {
            transform.SetParent(null);
            speed = -speed;
            Flip();
            Debug.DrawLine(transform.position, groundCheckerTarget.position, Color.red);
            return false;
        }
    }
    void Flip()
    {
        if (transform.position.x < groundCheckerTarget.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (transform.position.x > groundCheckerTarget.position.x)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            TakeDamage(collision.gameObject);
        }
    }

    void TakeDamage(GameObject obj)
    {
        obj.GetComponent<Player>().TakeDamage();
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            collider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bounceForce, ForceMode2D.Impulse);
            Destroy(gameObject);
        }
    }
}
